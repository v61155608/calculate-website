

  // function checkAndRunOnce() {
  //   console.log("run once");
  //   if (!localStorage.getItem('hasRunOnce')) {
  //     // The function hasn't run yet, so execute it
  //     window.location.href = 'login.html';

  //     // Set a flag in localStorage to indicate that it has run
  //     localStorage.setItem('hasRunOnce', true);
  //   }
  // }
  function removesession(){
    localStorage.setItem('hasRunOnce', false);
  }
  
const changeColspan = () => {
  const screenWidth = window.innerWidth;
  const titleLength = document.getElementsByClassName('colspan-title').length;
  const highlightLength = document.getElementsByClassName('colspan-highlight').length;

  if (screenWidth <= 1016) {
    for (let i = 0; i < titleLength; i++) {
      document.getElementsByClassName('colspan-title')[i].colSpan = '2'
    }
    for (let i = 0; i < highlightLength; i++) {
      document.getElementsByClassName('colspan-highlight')[i].colSpan = '1'
    }
  } else {
    console.log("desktop")
    for (let i = 0; i < titleLength; i++) {
      document.getElementsByClassName('colspan-title')[i].colSpan = '8'
    }
    for (let i = 0; i < highlightLength; i++) {
      document.getElementsByClassName('colspan-highlight')[i].colSpan = '7'
    }
  }
}
function navigateToURL(url) {
  window.location.href = url;
}
window.addEventListener("load", changeColspan)
window.addEventListener("resize", changeColspan)

document.addEventListener("DOMContentLoaded", function() {
  var userImages = document.getElementsByClassName("user-image");
  
  for (var i = 0; i < userImages.length; i++) {
      userImages[i].addEventListener("click", function() {
          var dropdown = this.nextElementSibling;
          toggleDropdown(dropdown);
      });
  }
});

function toggleDropdown(dropdown) {
  if (dropdown.style.display === "block") {
      dropdown.style.display = "none";
  } else {
      dropdown.style.display = "block";
  }
}
